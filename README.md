[![Doodba deployment](https://img.shields.io/badge/deployment-doodba-informational)](https://github.com/Tecnativa/doodba)
[![Last template update](https://img.shields.io/badge/last%20template%20update-v6.1.5-informational)](https://github.com/Tecnativa/doodba-copier-template/tree/v6.1.5)
[![Odoo](https://img.shields.io/badge/odoo-v17.0-a3478a)](https://github.com/odoo/odoo/tree/17.0)
[![pipeline status](https://gitlab.com/moduon/moduon-odoo-addons/badges/17.0/pipeline.svg)](https://gitlab.com/moduon/moduon-odoo-addons/commits/17.0)
[![coverage report](https://gitlab.com/moduon/moduon-odoo-addons/badges/17.0/coverage.svg)](https://gitlab.com/moduon/moduon-odoo-addons/commits/17.0)
[![LGPL-3.0-or-later license](https://img.shields.io/badge/license-LGPL--3.0--or--later-success})](LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)

# moduon-odoo-addons - a Doodba deployment

This project is a Doodba scaffolding. Check upstream docs on the matter:

- [General Doodba docs](https://github.com/Tecnativa/doodba).
- [Doodba copier template docs](https://github.com/Tecnativa/doodba-copier-template)
- [Doodba QA docs](https://github.com/Tecnativa/doodba-qa)

# Credits

This project is maintained by: Moduon
