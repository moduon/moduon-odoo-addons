import hashlib

from odoo import api, models


class ProjectProject(models.Model):
    _inherit = "project.project"

    @api.model
    def auto_update_from_jira(self):
        projects = self._get_projects_to_sync()
        max_retries = len(projects) + 1  # Allow to sync all projects
        for project in projects:
            ident_key = f"JIRA UPDATE PROJECT - {project.jira_key} - {project.jira_id}"
            project.with_delay(
                max_retries=max_retries,
                channel="root.jira_management",
                identity_key=hashlib.md5(ident_key.encode("utf-8")).hexdigest(),
                description=f"[JIRA] UPDATE PROJECT {project.jira_key}",
            ).update_from_jira()

    @api.model
    def auto_update_tasks_from_jira(self):
        projects = self._get_projects_to_sync()
        max_retries = len(projects) + 1  # Allow to sync all projects
        for project in projects:
            ident_key = f"JIRA UPDATE TASKS - {project.jira_key} - {project.jira_id}"
            project.with_delay(
                max_retries=max_retries,
                channel="root.jira_management",
                identity_key=hashlib.md5(ident_key.encode("utf-8")).hexdigest(),
                description=f"[JIRA] UPDATE PROJECT TASKS {project.jira_key}",
            ).update_tasks_from_jira()

    @api.model
    def auto_update_worklogs_from_jira(self):
        projects = self._get_projects_to_sync()
        max_retries = len(projects) + 1  # Allow to sync all projects
        for project in projects:
            ident_key = f"JIRA UPDATE WORKLOGS - {project.jira_key} - {project.jira_id}"
            project.with_delay(
                max_retries=max_retries,
                channel="root.jira_management",
                identity_key=hashlib.md5(ident_key.encode("utf-8")).hexdigest(),
                description=f"[JIRA] UPDATE PROJECT WORKLOGS {project.jira_key}",
            ).update_worklogs_from_jira()
