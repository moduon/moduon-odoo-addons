# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo.tests.common import TransactionCase


class TestBatchPicking(TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.stock_loc = cls.env.ref("stock.stock_location_stock")
        cls.customer_loc = cls.env.ref("stock.stock_location_customers")
        cls.product_category = cls.env["product.category"].create(
            {
                "name": "Test Category",
            }
        )
        cls.uom_unit = cls.env.ref("uom.product_uom_unit")
        cls.uom_kg = cls.env.ref("uom.product_uom_kgm")
        cls.group_fields = cls.env.ref("stock_picking_batch_ux.by_uom_product_categ")
        cls.picking_type_out = cls.env.ref("stock.picking_type_out")
        cls.product_test1 = cls._create_product("Product Test 1", cls.uom_unit)
        cls.product_test2 = cls._create_product("Product Test 2", cls.uom_kg)
        cls.product_test3 = cls._create_product("Product Test 3", cls.uom_unit)
        cls.product_test4 = cls._create_product("Product Test 4", cls.uom_kg)
        cls.picking_model = cls.env["stock.picking"]
        cls.batch_model = cls.env["stock.picking.batch"]
        cls.picking = cls._create_picking(cls.product_test1.ids + cls.product_test2.ids)
        cls.picking2 = cls._create_picking(
            cls.product_test3.ids + cls.product_test4.ids
        )

    @classmethod
    def _create_product(cls, name, uom_id):
        return cls.env["product.product"].create(
            {
                "name": name,
                "type": "product",
                "uom_id": uom_id.id,
                "uom_po_id": uom_id.id,
                "categ_id": cls.product_category.id,
            }
        )

    @classmethod
    def _create_picking(cls, product_ids):
        return cls.picking_model.with_context(planned=True).create(
            {
                "picking_type_id": cls.picking_type_out.id,
                "location_id": cls.stock_loc.id,
                "location_dest_id": cls.customer_loc.id,
                "move_ids": [
                    (
                        0,
                        0,
                        {
                            "name": "Test move",
                            "product_id": product_id,
                            "product_uom_qty": 1,
                            "location_id": cls.stock_loc.id,
                            "location_dest_id": cls.customer_loc.id,
                        },
                    )
                    for product_id in product_ids
                ],
                "move_line_ids": [
                    (
                        0,
                        0,
                        {
                            "product_id": product_id,
                            "qty_done": 1,
                            "location_id": cls.stock_loc.id,
                            "location_dest_id": cls.customer_loc.id,
                        },
                    )
                    for product_id in product_ids
                ],
            }
        )

    def test_stock_add_to_wave_01(self):
        """From pickings, wave existing."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        wave = self.batch_model.create(
            {
                "is_wave": True,
            }
        )
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=(self.picking | self.picking2).ids,
                active_model="stock.picking",
            )
            .create({"mode": "existing", "wave_id": wave.id})
        )
        action = wizard.attach_pickings()
        self.assertEqual(action["res_model"], "stock.move.line")
        self.assertEqual(
            action["domain"],
            [
                ("picking_id", "in", (self.picking | self.picking2).ids),
                ("state", "!=", "done"),
            ],
        )

    def test_stock_add_to_wave_02(self):
        """From pickings, wave new."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=(self.picking | self.picking2).ids,
                active_model="stock.picking",
            )
            .create({"mode": "new"})
        )

        action = wizard.attach_pickings()
        self.assertEqual(action["res_model"], "stock.move.line")
        self.assertEqual(
            action["domain"],
            [
                ("picking_id", "in", (self.picking | self.picking2).ids),
                ("state", "!=", "done"),
            ],
        )

    def test_stock_add_to_wave_03(self):
        """From move lines, wave existing."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        move_lines = (self.picking | self.picking2).move_line_ids
        wave = self.batch_model.create(
            {
                "is_wave": True,
            }
        )
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=move_lines.ids,
                active_model="stock.move.line",
            )
            .create({"mode": "existing", "wave_id": wave.id})
        )
        wizard.attach_pickings()
        self.assertEqual(wave.picking_ids, move_lines.picking_id)

    def test_stock_add_to_wave_04(self):
        """From move lines, wave new, no groupby."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        move_lines = (self.picking | self.picking2).move_line_ids
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=move_lines.ids,
                active_model="stock.move.line",
            )
            .create(
                {
                    "mode": "new",
                }
            )
        )
        wizard.attach_pickings()
        self.assertEqual(len(move_lines.mapped("batch_id")), 1)
        self.assertEqual(move_lines.mapped("batch_id").is_wave, True)

    def test_stock_add_to_wave_05(self):
        """From move lines, wave new, groupby but no fields."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        move_lines = (self.picking | self.picking2).move_line_ids
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=move_lines.ids,
                active_model="stock.move.line",
            )
            .create(
                {
                    "mode": "new",
                    "wave_by_group": True,
                }
            )
        )
        wizard.attach_pickings()
        self.assertEqual(len(move_lines.mapped("batch_id")), 1)
        self.assertEqual(move_lines.mapped("batch_id").is_wave, True)

    def test_stock_add_to_wave_06(self):
        """From move lines, wave new, groupby with fields."""
        self.picking.action_confirm()
        self.picking2.action_confirm()
        move_lines = (self.picking | self.picking2).move_line_ids
        field_uom = self.env.ref("stock.field_stock_move_line__product_uom_id")
        field_product_category = self.env.ref(
            "stock.field_stock_move_line__product_category_name"
        )
        wizard = (
            self.env["stock.add.to.wave"]
            .with_context(
                active_ids=move_lines.ids,
                active_model="stock.move.line",
            )
            .create(
                {
                    "mode": "new",
                    "wave_by_group": True,
                    "batch_group_field_id": self.group_fields.id,
                }
            )
        )
        wizard.attach_pickings()
        waves = move_lines.mapped("batch_id")
        self.assertEqual(len(waves), 2)
        for wave in waves:
            self.assertEqual(len(wave.move_line_ids.mapped(field_uom["name"])), 1)
            self.assertEqual(
                len(set(wave.move_line_ids.mapped(field_product_category["name"]))), 1
            )
