# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import fields, models


class BatchGroupField(models.Model):
    _name = "batch.group.field"
    _description = "Batch Group Field"
    _order = "sequence, id"

    name = fields.Char(required=True, translate=True)
    active = fields.Boolean(default=True)
    sequence = fields.Integer(default=0)
    line_ids = fields.One2many("batch.group.field.lines", "group_field_id")


class BatchGroupFieldLines(models.Model):
    _name = "batch.group.field.lines"
    _description = "Batch Group Field Lines"
    _order = "sequence, id"

    group_field_id = fields.Many2one("batch.group.field")
    sequence = fields.Integer(help="Group by move field", default=0)
    field_id = fields.Many2one(
        comodel_name="ir.model.fields",
        string="Field to group",
        domain=[
            ("model", "=", "stock.move.line"),
            ("store", "=", True),
        ],
        required=True,
        ondelete="cascade",
    )
