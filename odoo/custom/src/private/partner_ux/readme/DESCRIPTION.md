Improvements on Partner UX:

- Allows to set permissions for partner removal using the group "Contact Deletion",
  after the module installation the users wich have this permission can delete the
  partner
- Warning for missing fields on partner, in this moment the function check the country
  field
- Moved Industry below Categories
- Added `ref` field to partner tree view
