Improvements on Sale Invoice Management:

- Adds menu _Sales > Invoices_ to show Out Invoices, Credit Notes and Receipts.
