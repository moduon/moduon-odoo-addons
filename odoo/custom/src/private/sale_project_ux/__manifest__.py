# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Sale Project UX",
    "summary": "Improvements UX about Sale Project",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Services/Project",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["EmilioPascual"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "depends": [
        "sale_project",
    ],
    "data": ["views/sale_order_views.xml"],
}
