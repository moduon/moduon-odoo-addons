To use this module, you need to:

Sale order line

1. Go to Sales > Products > Products.
2. Create new service product and set Project in Create on Order Project.
3. Go to Sales > Orders > Quotations.
4. Create new quotation.
5. In order lines select product created before.
6. In Generated project column select and project already created.
7. Confirm quotation.
8. New project is not generated, sale order is linked to selected project.

Product product

1. Go to Sales > Products > Products.
2. Create new service product.
3. Set Create on Order to Project, set Project Template with one already created and
   save.
4. Set Create on Order to Task and set a Project already created. Save.
5. Chatter shows changes in Create on Order, Project and Project Template fields.
