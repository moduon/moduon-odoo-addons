from odoo import fields, models


class ProductAttribute(models.Model):
    _inherit = "product.attribute"

    create_variant = fields.Selection(
        default="no_variant",
    )
