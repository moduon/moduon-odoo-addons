from odoo import api, fields, models


class StockWarehouseOrderpoint(models.Model):
    _inherit = "stock.warehouse.orderpoint"

    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )

    @api.depends("product_id.responsible_id")
    def _compute_product_responsible_id(self):
        for op in self:
            op.product_responsible_id = op.product_id.responsible_id
