from odoo import api, fields, models


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    _order = (
        "location_sequence asc, location_dest_sequence asc, "
        "result_package_id asc, product_id asc, picking_partner_name asc, id"
    )

    product_barcode = fields.Char(
        compute="_compute_product_barcode",
        store=True,
        help="Product Barcode when the move line was created",
    )
    location_sequence = fields.Integer(
        related="location_id.sequence",
        string="Location Sequence",
        readonly=True,
        store=True,
        help="Sequence of the source location",
    )
    location_dest_sequence = fields.Integer(
        related="location_dest_id.sequence",
        string="Destination Location Sequence",
        readonly=True,
        store=True,
        help="Sequence of the destination location",
    )
    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )
    picking_partner_name = fields.Char(
        string="Contact name",
        related="picking_partner_id.name",
        store=True,
    )
    # Inherited fields
    picking_partner_id = fields.Many2one(store=True)

    def get_seller_product_display_name(self):
        self.ensure_one()
        seller_product_display_name = dict(
            self.with_context(
                partner_id=self.picking_partner_id.id,
                quantity=self.reserved_uom_qty,
                company_id=self.company_id.id,
            ).product_id.name_get()
        ).get(self.product_id.id)
        if not seller_product_display_name:
            return None
        if seller_product_display_name == self.product_id.display_name:
            return None
        return seller_product_display_name

    @api.depends("product_id")
    def _compute_product_barcode(self):
        for record in self:
            record.product_barcode = record.product_id.barcode

    @api.depends("product_id.responsible_id")
    def _compute_product_responsible_id(self):
        for line in self:
            line.product_responsible_id = line.product_id.responsible_id
