from odoo import api, fields, models


class StockLot(models.Model):
    _inherit = "stock.lot"

    product_responsible_id = fields.Many2one(
        "res.users",
        "Product Responsible",
        compute="_compute_product_responsible_id",
        precompute=True,
        readonly=True,
        store=True,
    )

    @api.depends("product_id.responsible_id")
    def _compute_product_responsible_id(self):
        for lot in self:
            lot.product_responsible_id = lot.product_id.responsible_id
