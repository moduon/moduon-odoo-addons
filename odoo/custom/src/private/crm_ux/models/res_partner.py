# Copyright 2023 Moduon Team S.L.
# License Other proprietary

from odoo import fields, models

CRM_TYPES = {
    "lead": "Lead",
    "opportunity": "Opportunity",
    "customer": "Customer",
}


class ResPartner(models.Model):
    _inherit = "res.partner"

    crm_type = fields.Selection(
        selection=list(CRM_TYPES.items()),
        compute="_compute_crm_type",
        search="_search_crm_type",
        help="Lead: Only has leads.\n"
        "Opportunity: Has opportunities but no leads.\n"
        "Customer: Has invoices.\n",
    )

    def _get_crm_type_classification(self) -> dict:
        """Returns partners classified by CRM Type"""
        all_partners = self.with_context(active_test=False).search(
            [("id", "child_of", self.ids)]
        )
        all_partners.read(["parent_id"])
        cl_model = self.env["crm.lead"].with_context(active_test=False).sudo()

        base_domain = [("partner_id", "child_of", all_partners.ids)]
        field_and_groupby = ["partner_id"]

        opportunity_data = cl_model.read_group(
            domain=base_domain + [("type", "=", "opportunity")],
            fields=field_and_groupby,
            groupby=field_and_groupby,
        )
        lead_data = cl_model.read_group(
            domain=base_domain + [("type", "in", ["lead", False])],
            fields=field_and_groupby,
            groupby=field_and_groupby,
        )

        possible_leads = self.browse()
        possible_opportunities = self.browse()

        for group in opportunity_data:
            partner = self.browse(group["partner_id"][0])
            while partner:
                if partner in self:
                    possible_opportunities |= partner
                partner = partner.parent_id

        for group in lead_data:
            partner = self.browse(group["partner_id"][0])
            while partner:
                if partner in self:
                    possible_leads |= partner
                partner = partner.parent_id

        customers = self.filtered(lambda c: c.customer_rank > 0)
        leads = possible_leads - possible_opportunities - customers
        opportunities = possible_opportunities - customers
        other = all_partners - leads - opportunities - customers
        return {
            "lead": leads,
            "opportunity": opportunities,
            "customer": customers,
            False: other,
        }

    def _compute_crm_type(self):
        """Compute the crm type of the partner"""
        for crmtype, partners in self._get_crm_type_classification().items():
            partners.crm_type = crmtype

    def _search_crm_type(self, operator: str, value):
        """Return records that match operator and value"""
        if operator not in {"=", "!="}:
            raise ValueError(f"Invalid operator {operator}")
        in_operator = "in" if operator == "=" else "not in"

        if value not in CRM_TYPES:
            raise ValueError(f"Invalid value {value}")

        domain = []
        crm_type_classification = self.search([])._get_crm_type_classification()
        for crmtype, partners in crm_type_classification.items():
            if crmtype == value:
                domain = [("id", in_operator, partners.ids)]
                break
        return domain
