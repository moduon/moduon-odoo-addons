# Copyright 2023 Moduon Team S.L.
# License Other proprietary

import logging

from odoo import SUPERUSER_ID, api

_logger = logging.getLogger(__name__)


def post_init_hook(cr, registry):
    """Translate some menus and actions properly"""
    env = api.Environment(cr, SUPERUSER_ID, {})
    # Lead action
    action_lead = env.ref("crm.crm_lead_all_leads")
    action_lead.with_context(lang="es_ES").name = "Iniciativas"
    # Lead menus
    menu_lead = env.ref("crm.crm_menu_leads")
    menu_lead_report = env.ref("crm.crm_opportunity_report_menu_lead")
    (menu_lead | menu_lead_report).with_context(lang="es_ES").name = "Iniciativas"
    # IAP Lead Mining menu
    try:
        menu_iap = env.ref(
            "crm_iap_mine.crm_iap_lead_mining_request_menu_action",
            raise_if_not_found=True,
        )
        menu_iap.with_context(
            lang="es_ES"
        ).name = "Solicitudes de minado de iniciativas"
    except ValueError:
        _logger.info("`crm_iap_mine` module not installed")
