Improvements on CRM UX:

- Add Date Deadline to search filters
- On Leads and Opportunities, `partner_id` field label changed to _Contact_
- New field _Commercial Partner_ on Leads and Opportunities
- Hide Company and Contact related info if Partner is set on Lead and Opportunity Forms
- Tracks changes on Contact field on Lead and Opportunity
- Shows Partner Industry on Lead and Opportunity Forms
- New filters on Contacts that allows to filter Leads, Opportunities and Customer CRM
  types
- Smart Button on Sale Order to show the related Opportunity
- Allows to cancel all Quotations when marking as lost an Opportunity
- Translation improvements
