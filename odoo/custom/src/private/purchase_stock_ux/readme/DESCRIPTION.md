Improvements on Purchase Stock UX:

- Add filter "My Products" when responsible in product is the user in:
  - Supplier Pricelist (for example in Purchase \> Configuration \> Vendor Pricelist,
    only if you have Administrator Purchase access).
  - Purchase Order (for example in Purchase \> Orders \> Purchase Order).
  - Purchase Order Line (Odoo not have access to this search; if you want test, then
    install
    [purchase_order_line_menu](https://github.com/OCA/purchase-workflow/tree/16.0/purchase_order_line_menu)).
  - Purchase Report (for example in Purchase \> Reporting \> Purchase, only if you have
    Administrator Purchase access).
- Add quick search and group by product responsible on following models:
  - Supplier Pricelist (for example in Purchase \> Configuration \> Vendor Pricelist,
    only if you have Administrator Purchase access).
  - Purchase Report (for example in Purchase \> Reporting \> Purchase, only if you have
    Administrator Purchase access).
- Add quick search and group by vendor in orderpoint search (for example in Inventory \>
  Operations \> Replenishment, only if you have Inventory Administrator access).
- Remove Hour from Arrival Date field on Purchase RFQ and PO reports.
