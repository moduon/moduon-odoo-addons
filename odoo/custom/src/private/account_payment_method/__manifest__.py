# Copyright 2022 Moduon
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

{
    "name": "Account Payment Method",
    "version": "16.0.2.0.2",
    "author": "Moduon",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "license": "LGPL-3",
    "category": "Accounting/Accounting",
    "depends": [
        "account",
        # Dependemos exclusivamente de éste módulo por el campo
        # property_payment_method_id
        "account_check_printing",
    ],
    "conflicts": ["account_payment_mode"],
    "data": [
        "security/ir.model.access.csv",
        "data/account_payment_method.xml",
        "views/account_payment_method_views.xml",
        "views/res_partner_views.xml",
        "views/account_move_views.xml",
        "views/account_payment_method_line_views.xml",
        "views/report_invoice.xml",
    ],
    "installable": False,
    "auto_install": True,
}
