from odoo import fields
from odoo.tests import Form, tagged

from odoo.addons.account.tests.common import AccountTestInvoicingCommon


@tagged("post_install", "-at_install")
class TestAccountMovePaymentMethodCommon(AccountTestInvoicingCommon):
    @classmethod
    def setUpClass(cls, chart_template_ref=None):
        super().setUpClass(chart_template_ref=chart_template_ref)

        cls.pay_method_in = cls.env.ref("account.account_payment_method_manual_in")
        cls.pay_method_out = cls.env.ref("account.account_payment_method_manual_out")

        cls.partner_a.property_customer_payment_method_id = cls.pay_method_in.id
        cls.partner_a.property_payment_method_id = cls.pay_method_out.id
        cls.partner_b.property_customer_payment_method_id = cls.pay_method_in.id
        cls.partner_b.property_payment_method_id = cls.pay_method_out.id

        cls.bank_journal = cls.company_data["default_journal_bank"]
        cls.bank_journal_2 = cls.company_data["default_journal_bank"].copy()
        cls.comp_bank_account1 = cls.env["res.partner.bank"].create(
            {
                "acc_number": "985632147",
                "partner_id": cls.env.company.partner_id.id,
                "acc_type": "bank",
            }
        )
        cls.comp_bank_account2 = cls.env["res.partner.bank"].create(
            {
                "acc_number": "741258963",
                "partner_id": cls.env.company.partner_id.id,
                "acc_type": "bank",
            }
        )
        cls.bank_journal.bank_account_id = cls.comp_bank_account1
        cls.bank_journal_2.bank_account_id = cls.comp_bank_account2
        cls.pay_method_in.print_bank_account = True

    def test_invoice_payment_method(self):
        """Invoices get appropriate preferred payment methods."""
        params = (
            ("out_invoice", self.pay_method_in),
            ("in_invoice", self.pay_method_out),
            ("in_refund", self.pay_method_in),
            ("out_refund", self.pay_method_out),
            ("out_receipt", self.pay_method_in),
            ("in_receipt", self.pay_method_out),
        )
        for move_type, expected_payment_method in params:
            with self.subTest(
                move_type=move_type, expected_payment_method=expected_payment_method
            ):
                invoice = self.init_invoice(
                    move_type, products=self.product_a + self.product_b
                )
                self.assertEqual(
                    invoice.preferred_payment_method_id, expected_payment_method
                )

    def test_creating_invoice(self):
        """If user overrides default, it is respected."""
        credit_card_out = self.env.ref(
            "account_payment_method.account_payment_method_credit_card_out"
        )
        move_f = Form(
            self.env["account.move"].with_context(
                default_move_type="out_invoice",
                account_predictive_bills_disable_prediction=True,
            )
        )
        move_f.invoice_date = fields.Date.from_string("2019-01-01")
        # Choosing a partner updates move preferred payment method
        move_f.partner_id = self.partner_a
        self.assertEqual(
            move_f.preferred_payment_method_id,
            self.partner_a.property_customer_payment_method_id,
        )
        # If the user chooses to alter it, that's kept
        move_f.preferred_payment_method_id = credit_card_out
        self.assertEqual(move_f.preferred_payment_method_id, credit_card_out)
        move = move_f.save()
        self.assertEqual(move.preferred_payment_method_id, credit_card_out)

    def test_manual_payment_method_available(self):
        """Create a new payment method and check if it's available in the Journal"""
        aj_bank = self.company_data["default_journal_bank"]
        payment_method = (
            self.env["account.payment.method"]
            .sudo()
            .create(
                {
                    "name": "Test Payment Method",
                    "code": "manual_in_test",
                    "payment_type": "inbound",
                }
            )
        )
        self.assertTrue(aj_bank._is_payment_method_available(payment_method.code))

    def test_get_bank_accounts_to_print(self):
        move_f = Form(
            self.env["account.move"].with_context(
                default_move_type="out_invoice",
                account_predictive_bills_disable_prediction=True,
            )
        )
        move_f.partner_id = self.partner_a
        move_f.invoice_date = fields.Date.from_string("2019-01-01")
        move = move_f.save()
        bank_accounts_move = move.get_bank_accounts_to_print()
        bank_accounts_journal = (
            self.bank_journal.bank_account_id | self.bank_journal_2.bank_account_id
        )
        self.assertEqual(bank_accounts_move, bank_accounts_journal)

    def test_change_customer_back_and_forth(self):
        # Partner B has different payment methods
        pay_in_b = self.pay_method_in.copy({"code": "b_in", "name": "b_in"})
        pay_out_b = self.pay_method_out.copy({"code": "b_out", "name": "b_out"})
        self.partner_b.property_customer_payment_method_id = pay_in_b
        self.partner_b.property_payment_method_id = pay_out_b
        params = (
            ("out_invoice", self.pay_method_in, pay_in_b),
            ("in_invoice", self.pay_method_out, pay_out_b),
            ("in_refund", self.pay_method_in, pay_in_b),
            ("out_refund", self.pay_method_out, pay_out_b),
            ("out_receipt", self.pay_method_in, pay_in_b),
            ("in_receipt", self.pay_method_out, pay_out_b),
        )
        for move_type, a_method, b_method in params:
            with self.subTest(move_type=move_type):
                invoice = self.init_invoice(move_type, partner=self.partner_a)
                with Form(invoice) as inv_f:
                    self.assertEqual(inv_f.preferred_payment_method_id, a_method)
                    inv_f.partner_id = self.partner_b
                    self.assertEqual(inv_f.preferred_payment_method_id, b_method)
                    inv_f.partner_id = self.partner_a
                    self.assertEqual(inv_f.preferred_payment_method_id, a_method)
