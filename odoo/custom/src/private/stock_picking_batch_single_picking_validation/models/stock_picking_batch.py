# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import api, models


class StockPickingBatch(models.Model):
    _inherit = "stock.picking.batch"

    @api.depends("company_id", "picking_type_id", "state")
    def _compute_allowed_picking_ids(self):
        res = super()._compute_allowed_picking_ids()
        if self.env.context.get("forced_allowed_picking_ids"):
            self.allowed_picking_ids += self.env["stock.picking"].browse(
                self.env.context.get("forced_allowed_picking_ids")
            )
        return res

    def _sanity_check(self):
        if not self.env.context.get("forced_allowed_picking_ids"):
            return super()._sanity_check()
