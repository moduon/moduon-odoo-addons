To use this module, you need to:

1.  Create a Lead Template from CRM/Configuration/Lead Templates if you don't have any.
2.  Create a new Lead or go to an existing one and select a Lead Template from top of
    the from.
3.  Create a new Opportunity from Kanban and fill Lead Template instead of Oppportunity
    name.
4.  Check all the fields are being filled with the template values.
