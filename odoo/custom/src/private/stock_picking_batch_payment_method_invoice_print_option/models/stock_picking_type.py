# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)


from odoo import fields, models


class StockPickingType(models.Model):
    _inherit = "stock.picking.type"

    account_payment_method_ids = fields.Many2many(
        "account.payment.method",
        string="Payment Methods",
        help="Payment methods that will be used to filter the invoices to print."
        " If empty, all the invoices will be printed.",
        domain="[('payment_type', '=', 'inbound')]",
    )
