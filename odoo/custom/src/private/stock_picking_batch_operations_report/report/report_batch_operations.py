# Copyright 2023 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

from collections import OrderedDict, defaultdict
from operator import attrgetter

from odoo import api, fields, models


class ReportBatchOperations(models.AbstractModel):
    _name = "report.stock_picking_batch_operations_report.batch_operations"
    _description = "Report for Batch Picking Group by Product"

    @api.model
    def _subgroup_name(self, operation, batch):
        """Use one stock.move.line to get the name of the group."""
        batch.ensure_one()
        operation.ensure_one()
        name_list = []
        for field in batch.batch_group_field_line_ids.field_id:
            if field.ttype == "many2one":
                if operation[field.name]:
                    (
                        name_list.append(operation[field.name]["display_name"])
                        if (
                            "display_name" in operation[field.name]
                            and operation[field.name]["display_name"]
                        )
                        else name_list.append(operation[field.name]["name"])
                    )
            elif field.ttype in ("boolean", "integer", "float"):
                name_list.append(str(operation[field.name]))
            elif operation[field.name]:
                if field.ttype in ("datetime", "date"):
                    name_list.append(fields.Datetime.to_string(operation[field.name]))
                elif field.ttype == "many2many":
                    name_list.append(
                        ", ".join(
                            [f_relation.name for f_relation in operation[field.name]]
                        )
                    )
                else:
                    name_list.append(operation[field.name])
        return ", ".join(name_list) if name_list else ""

    @api.model
    def _lines_grouped(self, lines, *fnames):
        """Yield stock move lines grouped by chosen fields.

        Args:
            lines (Model<stock.move.line>):
                The stock move lines to group.
            *fnames (str):
                The field names to group by. Supports dotted sequences
                of fields, like `partner_id.name`.

        Yields:
            Recordsets that share the same value in all the chosen fields.
        """
        groups = defaultdict(lines.browse)
        for line in lines:
            groups[tuple(attrgetter(fname)(line) for fname in fnames)] |= line
        # Order group by its keys
        sorted_groups = OrderedDict(
            sorted(
                groups.items(),
                key=lambda item_tuple: [key_elem.id for key_elem in item_tuple[0]],
                reverse=False,
            )
        )
        yield from sorted_groups.values()

    @api.model
    def _batch_lines_grouped(self, batch):
        """Yield stock move lines grouped using the criteria selected by the user."""
        yield from self._lines_grouped(
            batch.move_line_ids,
            *(field.name for field in batch.batch_group_field_line_ids.field_id),
        )

    @api.model
    def _order_move_lines(self, move_lines, order: str = None):
        """Order move lines

        :param move_lines: Recordset of stock.move.line
        :param order: Order to apply to the search. If falsy, native order will be used
        """
        return self.env["stock.move.line"].search(
            [("id", "in", move_lines.ids)], order=order
        )

    @api.model
    def _get_lines(self, batch, move_lines):
        sml_group_list = list()
        if self.env.ref("uom.product_uom_categ_kgm") not in move_lines.mapped(
            "product_id.uom_id.category_id"
        ):
            sml_group_list = self._get_lines_wave_fixed_weight(move_lines)
        else:
            sml_group_list = self._get_lines_wave_variable_weight(move_lines)
        return sml_group_list

    @api.model
    def _get_lines_wave_fixed_weight(self, move_lines):
        sml_group = self.env["stock.move.line"]
        sml_group_list = list()
        for line in self._order_move_lines(move_lines):
            if len(sml_group) and line.product_id != sml_group[-1].product_id:
                sml_group_list.append(sml_group)
                sml_group = self.env["stock.move.line"]
            sml_group += line
        sml_group_list.append(sml_group)
        return sml_group_list

    @api.model
    def _get_lines_wave_variable_weight(self, move_lines):
        sml_group = self.env["stock.move.line"]
        sml_group_list = list()
        mls_sorted = self._order_move_lines(move_lines)
        while len(mls_sorted):
            line = mls_sorted[0]
            if len(sml_group) and (
                line.product_id != sml_group[-1].product_id
                or line.picking_partner_id != sml_group[-1].picking_partner_id
            ):
                sml_group_list.append(sml_group)
                sml_group = self.env["stock.move.line"]
            sml_group += line.move_id.move_line_ids
            mls_sorted -= line.move_id.move_line_ids
        sml_group_list.append(sml_group)
        return sml_group_list

    @api.model
    def _get_total(self, move_lines):
        (
            total_demand,
            total_done,
            total_packaging_demand,
            total_packaging_done,
            total_without_packaging_demand,
            total_without_packaging_done,
        ) = (
            0,
            0,
            {},
            {},
            0,
            0,
        )
        for line in move_lines:
            total_demand += (
                line.qty_done if line.state == "done" else line.reserved_uom_qty
            )
            total_done += line.qty_done if line.state == "done" else 0
            if line.move_id.product_packaging_id:
                packaging = line.move_id.product_packaging_id
                if packaging not in total_packaging_demand:
                    total_packaging_demand[
                        packaging
                    ] = packaging.product_uom_id._compute_quantity(
                        qty=(
                            line.qty_done
                            if line.state == "done"
                            else line.reserved_uom_qty
                        )
                        / packaging.qty,
                        to_unit=packaging.product_uom_id,
                    )
                    total_packaging_done[packaging] = line.product_packaging_qty_done
                else:
                    total_packaging_demand[
                        packaging
                    ] += packaging.product_uom_id._compute_quantity(
                        qty=(
                            line.qty_done
                            if line.state == "done"
                            else line.reserved_uom_qty
                        )
                        / packaging.qty,
                        to_unit=packaging.product_uom_id,
                    )
                    total_packaging_done[packaging] += line.product_packaging_qty_done
            else:
                total_without_packaging_demand += (
                    line.qty_done if line.state == "done" else line.reserved_uom_qty
                )
                total_without_packaging_done += (
                    line.qty_done if line.state == "done" else 0
                )
        return {
            "total_demand": total_demand,
            "total_done": total_done,
            "total_packaging_demand": total_packaging_demand,
            "total_packaging_done": total_packaging_done,
            "total_without_packaging_demand": total_without_packaging_demand,
            "total_without_packaging_done": total_without_packaging_done,
        }

    def _weight_sml(self, move_lines):
        """Calculate the weight of a group of stock move lines."""
        total_weight = 0.0
        for sml in move_lines.filtered(lambda sml: sml.product_id.weight > 0.00):
            if sml.state == "done":
                total_weight += sml.qty_done * sml.product_id.weight
            else:
                total_weight += sml.reserved_uom_qty * sml.product_id.weight
        return total_weight

    @api.model
    def _get_report_values(self, docids, data=None):
        model = "stock.picking.batch"
        docs = self.env[model].browse(docids)
        return {
            "batch_lines_grouped": self._batch_lines_grouped,
            "doc_ids": docids,
            "doc_model": model,
            "docs": docs,
            "lines_grouped": self._lines_grouped,
            "now": fields.Datetime.now,
            "subgroup_name": self._subgroup_name,
            "get_lines": self._get_lines,
            "get_total": self._get_total,
            "weight_sml": self._weight_sml,
        }
