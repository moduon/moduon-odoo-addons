# Copyright 2022 Moduon
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
from odoo import fields, models


class AccountTax(models.Model):
    _inherit = "account.tax"

    invoice_repartition_tag_ids = fields.Many2many(
        string="Invoice Tax Grids",
        related="invoice_repartition_line_ids.tag_ids",
    )
    refund_repartition_tag_ids = fields.Many2many(
        string="Refund Invoice Tax Grids",
        related="refund_repartition_line_ids.tag_ids",
    )
