import requests

from .common import Jira


class WorkLog(Jira):
    resource_type = "worklog"

    @Jira.manage_response
    def get_worklogs(self, ids):
        data = {
            "ids": ids,
        }
        return requests.post(
            self._get_base_resource_url("list"),
            headers=self._headers,
            auth=self._authorization,
            json=data,
            timeout=30,
        )

    @Jira.manage_response
    def get_updated_worklogs_ids(self, since):
        return requests.get(
            self._get_base_resource_url("updated"),
            headers=self._headers,
            auth=self._authorization,
            params={
                "since": since,
            },
            timeout=30,
        )

    @Jira.manage_response
    def get_deleted_worklogs_ids(self, since):
        return requests.get(
            self._get_base_resource_url("deleted"),
            headers=self._headers,
            auth=self._authorization,
            params={
                "since": since,
            },
            timeout=30,
        )
