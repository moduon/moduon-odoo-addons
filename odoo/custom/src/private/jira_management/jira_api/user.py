import requests

from .common import Jira


class User(Jira):
    resource_type = "user"

    @Jira.manage_response
    def get(self, username):
        return requests.get(
            self._get_base_resource_url(),
            headers=self._headers,
            auth=self._authorization,
            params={"username": username, "includeDeleted": True},
            timeout=30,
        )

    @Jira.manage_response
    def search(self, username=None, query=None, max_results=5):
        if not (username or query):
            raise Exception("Missing parameters")

        params = {"maxResults": max_results}
        if username:
            params.update({"username": username})
        if query:
            params.update({"query": query})

        return requests.get(
            self._get_base_resource_url("search"),
            headers=self._headers,
            auth=self._authorization,
            params=params,
            timeout=30,
        )
