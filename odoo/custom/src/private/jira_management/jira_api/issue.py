import requests

from .common import Jira


class Issue(Jira):
    resource_type = "issue"

    # pylint: disable=W8106
    @Jira.manage_response
    def create(
        self,
        project,
        summary,
        issuetype,
        description=None,
        epic_key=None,
        asignee=None,
        labels=None,
    ):
        issue_description = description and self.markup_to_content(description) or []
        data = {
            "fields": {
                "summary": summary,
                "parent": {"key": epic_key},
                "issuetype": {"id": issuetype},
                "project": {"key": project},
                "description": {
                    "type": "doc",
                    "version": 1,
                    "content": issue_description,
                },
                "labels": labels
                and [label.replace(" ", "") for label in labels]
                or None,
                "assignee": {"id": asignee},
            },
        }
        return requests.post(
            self._get_base_resource_url(),
            headers=self._headers,
            auth=self._authorization,
            json=data,
            timeout=30,
        )

    @Jira.manage_response
    def get(self, key):
        return requests.get(
            self._get_base_resource_url(resource=key),
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_response
    def transition(self, key, transition):
        data = {
            "transition": {
                "id": transition["id"],
            }
        }
        return requests.post(
            self._get_base_resource_url("transitions", resource=key),
            headers=self._headers,
            auth=self._authorization,
            json=data,
            timeout=30,
        )

    @Jira.manage_response
    def available_transitions(self, key):
        return requests.get(
            self._get_base_resource_url("transitions", resource=key),
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_response
    def comments(self, key):
        return requests.get(
            self._get_base_resource_url("comment", resource=key),
            headers=self._headers,
            auth=self._authorization,
            timeout=30,
        )

    @Jira.manage_paginated_response("worklogs")
    def worklog(self, key, **kwargs):
        return requests.get(
            self._get_base_resource_url("worklog", resource=key),
            headers=self._headers,
            auth=self._authorization,
            params=dict(**kwargs),
            timeout=30,
        )
