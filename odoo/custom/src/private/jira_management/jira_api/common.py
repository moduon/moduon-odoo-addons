import logging

from lxml import etree
from requests.auth import HTTPBasicAuth

from odoo import _

from . import jira_exceptions as jex

_logger = logging.getLogger(__name__)


class Jira:
    api_version = 3

    def __init__(
        self,
        url,
        username,
        token,
        company,
        project_type=None,
        project_template=None,
        *args,
        **kwargs,
    ):
        self._url = url
        self._base_resource_url = f"{url}/rest/api/{Jira.api_version}"

        self._authorization = HTTPBasicAuth(username, token)
        self._headers = {"Accept": "application/json"}

        self._project_type = project_type
        self._project_template = project_template
        self._company = company

    @classmethod
    def from_company(cls, company, *args, **kwargs):
        su_company = company.sudo()
        return cls(
            su_company.atlassian_url,
            su_company.jira_auth_username,
            su_company.jira_auth_token,
            su_company,
            project_type=company.jira_default_project_template.jira_type or None,
            project_template=company.jira_default_project_template.code or None,
            args=args,
            kwargs=kwargs,
        )

    def _get_base_resource_url(self, *post_urls, resource=None):
        url = f"{self._base_resource_url}/{self.resource_type}"
        if resource:
            url += "/" + resource
        if post_urls:
            url += "/" + "/".join(post_urls)
        return url

    def _manage_response_exception(self, response):
        extra_message = []
        try:
            json_resp = response.json()
            extra_message.extend(json_resp.get("errorMessages"))
            extra_message.extend(
                [f"{k}: {v}" for k, v in (json_resp.get("errors") or {}).items()]
            )
        except Exception:
            pass
        exception_message = "\n".join(extra_message)
        if response.status_code == 400:
            raise jex.RequestNotValidException(
                _("Request is not valid"), exception_message
            )
        elif response.status_code == 401:
            raise jex.UserNotLoggedIndException(
                _("User is not logged in"), exception_message
            )
        elif response.status_code == 403:
            raise jex.AccessRightsException(
                _("User doesn't have enough access rights"), exception_message
            )
        elif response.status_code == 404:
            raise jex.ResourceNotFoundException(
                _("Resource doesn't exist"), exception_message
            )
        else:
            raise Exception(response.text, exception_message)

    @staticmethod
    def manage_response(func, *args, **kwargs):
        def wrapper(slf, *args, **kwargs):
            response = func(slf, *args, **kwargs)
            if response.ok:
                try:
                    return response.json()
                except Exception:
                    return response.text
            return slf._manage_response_exception(response)

        return wrapper

    @staticmethod
    def manage_paginated_response(page_key):
        def manage_response(func, *args, **kwargs):
            def wrapper(slf, *args, **kwargs):
                initial_response = func(slf, *args, **kwargs)
                if initial_response.ok:
                    json_response = initial_response.json()
                    max_r = json_response.get("maxResults", 0)
                    total_results = json_response.get("total", 0)
                    if total_results <= max_r:
                        return json_response
                    for idx_page in range(total_results // max_r):
                        start_at = (idx_page + 1) * max_r
                        page_kwargs = dict(kwargs, startAt=start_at, maxResults=max_r)
                        page_response = func(slf, *args, **page_kwargs)
                        if page_response.ok:
                            page_json_response = page_response.json()
                            json_response[page_key].extend(page_json_response[page_key])
                    json_response["maxResults"] = len(json_response[page_key])
                    return json_response
                return slf._manage_response_exception(initial_response)

            return wrapper

        return manage_response

    @staticmethod
    def _markup_to_text(txt_element):
        block = []
        jira_tags = {
            "strong": "strong",
            "em": "em",
            "u": "underline",
            "s": "strike",
        }

        if txt_element.text:
            txt_block = {"text": txt_element.text, "type": "text"}
            if txt_element.tag == "pre":
                txt_block.update({"marks": [{"type": "code"}]})
            block.append(txt_block)

        for text_child in txt_element.getchildren():
            if text_child.tag == "br":
                continue

            txt_block = {"text": text_child.text, "type": "text", "marks": []}
            if jira_tags.get(text_child.tag):
                txt_block["marks"].append({"type": jira_tags.get(text_child.tag)})

            block.append(txt_block)
            if text_child.tail:
                block.append({"text": text_child.tail, "type": "text"})
        return block

    @staticmethod
    def _markup_to_paragraph(text_element, h_level=0):
        block = {
            "content": Jira._markup_to_text(text_element),
            "type": "paragraph",
        }
        if h_level:
            block.update(
                {
                    "attrs": {"level": h_level},
                    "type": "heading",
                }
            )
        return block

    @staticmethod
    def _markup_to_item_list(li_elements, bulleted=False):
        return {
            "type": bulleted and "bulletList" or "orderedList",
            "content": [
                {
                    "type": "listItem",
                    "content": [Jira._markup_to_paragraph(li)],
                }
                for li in li_elements
            ],
        }

    @staticmethod
    def markup_to_content(markup):
        content = []
        etree_markup = etree.fromstring(
            "<div>" + str(markup).replace("<br>", "<br/>") + "</div>"
        )
        for etree_child in etree_markup.getchildren():
            if etree_child.tag in ("p", "pre"):
                content.append(Jira._markup_to_paragraph(etree_child))
            elif etree_child.tag in ("h1", "h2", "h3", "h4", "h5", "h6"):
                content.append(
                    Jira._markup_to_paragraph(
                        etree_child, h_level=int(etree_child.tag[-1])
                    )
                )
            elif etree_child.tag in ("ul", "ol"):
                content.append(
                    Jira._markup_to_item_list(
                        etree_child.getchildren(),
                        bulleted=etree_child.tag == "ol",
                    )
                )
        return content
