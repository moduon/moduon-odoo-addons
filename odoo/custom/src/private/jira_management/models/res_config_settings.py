from urllib.parse import urlparse

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    atlassian_url = fields.Char(
        related="company_id.atlassian_url",
        readonly=False,
    )
    jira_auth_mode = fields.Selection(
        string="Jira Authentication",
        related="company_id.jira_auth_mode",
        readonly=False,
    )
    jira_auth_username = fields.Char(
        string="Jira Username",
        related="company_id.jira_auth_username",
        readonly=False,
    )
    jira_auth_token = fields.Char(
        string="Jira API Token",
        related="company_id.jira_auth_token",
        readonly=False,
    )
    jira_default_project_template = fields.Many2one(
        string="Default Jira Project Template",
        related="company_id.jira_default_project_template",
        readonly=False,
    )
    jira_default_project_employee_id = fields.Many2one(
        string="Default Jira Project Employee",
        related="company_id.jira_default_project_employee_id",
        readonly=False,
    )

    @api.onchange("atlassian_url")
    def onchage_atlassian_url(self):
        """Cleans Atlassian URL to be able to use it"""
        if self.atlassian_url:
            parse_uri = urlparse(self.atlassian_url)
            self.atlassian_url = f"{parse_uri.scheme}://{parse_uri.netloc}"
