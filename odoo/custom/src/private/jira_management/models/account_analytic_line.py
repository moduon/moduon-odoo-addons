from odoo import fields, models


class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"

    jira_id = fields.Integer(
        string="Jira ID",
        copy=False,
    )
    jira_write_date = fields.Char(
        string="Jira Updated",
    )
    top_parent_id = fields.Many2one(
        related="task_id.top_parent_id",
        store=True,
        readonly=True,
    )
    jira_issuetype_hierarchy_level = fields.Selection(
        related="task_id.jira_issuetype_hierarchy_level",
        readonly=True,
        store=True,
    )
    jira_top_issuetype_hierarchy_level = fields.Selection(
        related="top_parent_id.jira_issuetype_hierarchy_level",
        string="Top Parent Issue Type Hierarchy Level",
        readonly=True,
        store=True,
    )
    _sql_constraints = [
        (
            "jira_id_unique",
            "UNIQUE(jira_id, project_id, task_id, company_id)",
            "Jira Key must be unique per company.",
        ),
    ]
