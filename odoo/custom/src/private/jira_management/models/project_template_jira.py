from odoo import fields, models


class ProjectTemplateJira(models.Model):
    _name = "project.template.jira"
    _description = "Jira Project Template"

    jira_type = fields.Selection(
        selection=[
            ("business", "Business"),
            ("service_desk", "Service Desk"),
            ("software", "Software"),
        ],
        required=True,
    )
    code = fields.Char(
        required=True,
    )
    name = fields.Char(
        required=True,
    )
    sequence = fields.Integer()
    active = fields.Boolean()

    _sql_constraints = [
        (
            "code_unique",
            "UNIQUE(code)",
            "Code must be unique.",
        ),
    ]
