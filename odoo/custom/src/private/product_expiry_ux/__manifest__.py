# Copyright 2024 Moduon Team S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0)

{
    "name": "Product Expiry UX",
    "summary": "Improvements on Product Expiry",
    "version": "16.0.1.0.0",
    "development_status": "Alpha",
    "category": "Inventory/Inventory",
    "website": "https://gitlab.com/moduon/moduon-odoo-addons",
    "author": "Moduon",
    "maintainers": ["Shide"],
    "license": "LGPL-3",
    "application": False,
    "installable": False,
    "auto_install": True,
    "depends": ["product_expiry"],
    "data": [
        "views/report_delivery_document.xml",
    ],
}
