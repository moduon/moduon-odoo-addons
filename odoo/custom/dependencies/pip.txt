git+https://github.com/OCA/openupgradelib.git@master
unicodecsv
unidecode
asn1crypto

# l10n-spain
cryptography

# reporting-engine
xlsxwriter
xlrd
